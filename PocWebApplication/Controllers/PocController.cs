﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http;
using PocWebApplication.Models;
using System;
using System.Reflection;


namespace PocWebApplication.Controllers
{
    public class PocController : Controller
    {   
        public string str = string.Empty;
    
        public Dictionary<string, string> news = new Dictionary<string, string>();
        public Poc_reportmodelDataContext pr = new Poc_reportmodelDataContext();
        public int j = 0;
        public string  errormessage;
   
        //// GET: Poc
        ////[HttpGet]
        ////public ActionResult Index()
        ////{
        ////    return View();
        ////}
        //[HttpGet]
        //public async Task<ActionResult> Index()
        //{
        //    //for (int i = 1; i <=7; i++)
        //    //{
        //    //    if((Request.Form["att" + i] !=null )&&(Request.Form["value" + i] !=null))
        //    //    {
        //    //        news.Add(Request.Form["att" + i].Trim(), Request.Form["value" + i].Trim());
        //    //    }
        //    //    else
        //    //    {
        //    //        continue;
        //    //    }

        //    //}

        //    //foreach (KeyValuePair<string,string> kvkey in news)
        //    //{
        //    //    if ((kvkey.Key.Trim() != "Division") && (kvkey.Key.Trim()) != "BU")
        //    //    {
        //    //        bool t = pr.attributes.Any(x => x.attributename.Trim().Equals(kvkey.Key.Trim()));
        //    //        if (!t)
        //    //        {
        //    //            errormessage += kvkey.Key + " is not defined in the rule set ";
        //    //        }
        //    //        else
        //    //        { continue; }


        //    //    }
        //    //     else  
        //    //    {

        //    //        continue;
        //    //    }


        //    //}
        //    //   string path = @"C:\\Users\\189015\\Desktop\\Sample.xlsx";
        //    OleDbConnection Con = new OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0 ; Data Source='c:\\Users\\189015\\Desktop\\Sample.xlsx';Extended Properties='Excel 8.0;HDR=NO;IMEX=1;'");
        //    DataSet Dtset = new DataSet();
        //    OleDbDataAdapter cmd = new OleDbDataAdapter("Select * from [Sheet1$]",Con);
        //    cmd.TableMappings.Add("Table","TestTable");
        //    cmd.Fill(Dtset);
        //    Con.Close();
        //    DataTable dt = new DataTable();
        //    dt = Dtset.Tables[0];

        //    //foreach (DataRow dr in dt.Rows)
        //    //{
        //    //    news.Add(dr[0].ToString(), dr[1].ToString());

        //    //}
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        str += "s[" + j + "].Key=" + dr[0].ToString().Trim() + "&s[" + j + "].Value=" + dr[1].ToString().Trim();
        //        str += j != (dt.Rows.Count - 1) ? "&" : string.Empty;
        //        j++;

        //    }

        //    //foreach (KeyValuePair<string,string> kv in news)
        //    //    {
        //    //        str += "s[" +j+ "].Key=" + kv.Key + "&s[" +j+ "].Value=" + kv.Value;
        //    //        str += j != (news.Count-1) ? "&" : string.Empty;
        //    //         j++;
        //    //    }

        //    var uri = "http://localhost:55577/api/Poc/poc?" +str;
        //    var request = new HttpRequestMessage(HttpMethod.Get,uri );
        //    var client = new HttpClient();
        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    HttpResponseMessage response = await client.SendAsync(request);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        var o = await response.Content.ReadAsStringAsync();
        //        PocReport p = new PocReport();
        //        p = JsonConvert.DeserializeObject<PocReport>(o);
        //        pr.ExecuteCommand("DELETE FROM REPORTS");
        //        foreach (result item in p.report)
        //        {
                
        //            pr.insertReport(p.db.Division, p.db.Business_Unit, item.Attribute, item.Value, item.Rating);
                    
        //        }
        //        pr.SubmitChanges();
        //        SendEmail();
        //      TempData["report"] = p;
               
        //        return RedirectToAction("viewData", TempData["report"]);
        //    }
            

        //    return View();
        //}

        //public void SendEmail()
        //{
           
        //    string signature = "<br/><br/><br/><br/><br/>Regards,<br/>Akshay";
        //    MailMessage mm = new MailMessage("189015@nttdata.com", "189015@nttdata.com");
        //    mm.Subject = "Automate Policy peer review report";
        //    mm.Body = "Hi Akshay,<br/> The policy review has been completed for the below data points. You can proceed for the remaining data points <br/> <br/>"+signature; 
                
        //    mm.IsBodyHtml = true;
        // //  mm.Attachments.Add(new Attachment("c:\\Users\\189015\\Downloads\\Report.pdf"));
            
        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = "smtp.office365.com";
        //    //smtp.Port = 143;
        //    smtp.EnableSsl = true;


        //    smtp.UseDefaultCredentials = false;
        //    smtp.Credentials = new NetworkCredential("189015@nttdata.com", "Ak5$80088457");

        //    smtp.Send(mm);

        // }

 

        [HttpGet]
        public ActionResult addNewRule()
        {
            ViewData["divisions"] = new SelectList(pr.divisions,"name","name");
            ViewData["Bu"] = new SelectList(pr.businessUnits,"name","buName");
  
            
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> addNewRule(FormCollection nr)
        {
            ViewData["divisions"] = new SelectList(pr.divisions, "name", "name");
            ViewData["Bu"] = new SelectList(pr.businessUnits, "name", "buName");
            string divname= nr["divisions"].Trim();
            string buname= nr["Bu"].Trim();
            string val = nr["val"].Trim();
            string s = nr["rating"].Trim();
            string[] values = val.Split(',');
            string[] scores = s.Split(',');
            Type t = typeof(newRule);
            PropertyInfo[] p = t.GetProperties();

            for (int i = 0; i < values.Length; i++)
            {
                j = 0;
                Dictionary<string, string> adic = new Dictionary<string, string>();
                string astr = string.Empty;
                adic.Add(p[0].Name, divname);
                adic.Add(p[1].Name, buname);
                adic.Add(nr["att"], values[i]);
                adic.Add(p[4].Name, scores[i]);
                foreach (KeyValuePair<string, string> av in adic)
                {
                    astr += "a[" + j + "].Key=" + av.Key + "&a[" + j + "].Value=" + av.Value;
                    astr += j != (adic.Count - 1) ? "&" : string.Empty;
                    j++;
                }
                var auri = "http://localhost:8085/api/Poc/newrule?" + astr;
                var arequest = new HttpRequestMessage(HttpMethod.Post, auri);
                var aclient = new HttpClient();
                aclient.DefaultRequestHeaders.Accept.Clear();
                aclient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage aresponse = await aclient.SendAsync(arequest);
                if (aresponse.IsSuccessStatusCode)
                {
                    ViewBag.result = "Rule was created";


                }
                else { ViewBag.result = "Rule not created"; }

            }


           
            

  
             
            return View();
        }

        [HttpGet]
        public ActionResult addPolicy()
        {
            return View();


        }
        [HttpPost]
        public ActionResult addPolicy(FormCollection f)
        {
            Policy p = new Policy();
            Policy_ModelDataContext pdb = new Policy_ModelDataContext();
            p.division = f["_division"].Trim();
            p.businessUnit = f["_businessUnit"].Trim();
            p.underWriteName = f["_underWriteName"].Trim();
            p.u_wReference = f["_u_wReference"].Trim();
            p.brokerCode = f["_brokerCode"].Trim();
            p.assured = Request.Form["_assured"].Trim();
            p.writtenDate = Convert.ToDateTime(f["_writtenDate"]);
            p.periodFrom = Convert.ToDateTime(f["_periodFrom"]);
            p.periodTo = Convert.ToDateTime(f["_periodTo"]);
            p.Territory = f["_Territory"].Trim();
            p.Limit = f["_Limit"].Trim();
            p.Excess = f["_Excess"].Trim();
            p.Order = int.Parse(f["_Order"]);
            p.Deductions = (f["_Deductions"]).Trim();
            p.Interest = f["_Interest"].Trim();
            p.Premium_RTBL = int.Parse(f["_Premium_RTBL"]);
            p.Claims_RTBL = int.Parse(f["_Claims_RTBL"]);
            p.Limit_Deductible_RTBL = int.Parse(f["_Limit_Deductible_RTBL"]);
            p.Deduction_Change = int.Parse(f["_Deduction_Change"]);
            p.Overall_RTBL = int.Parse(f["_Overall_RTBL"]);
            p.Exposures = f["_Exposures"].Trim();
            p.status = f["_status"].Trim();
            p.WrittenLine = f["_WrittenLine"].Trim();


            pdb.insertPolicy(p.division, p.businessUnit, p.underWriteName.Trim(), p.u_wReference.Trim(), p.brokerCode.Trim(), p.assured.Trim(), p.writtenDate,p.periodFrom,
                
                               p.periodTo,p.Territory.Trim(), p.Limit.Trim(), p.Excess.Trim(), p.Order,p.Deductions.Trim(), p.Interest.Trim(), p.Premium_RTBL,p.Claims_RTBL,
                               
                                p.Limit_Deductible_RTBL,p.Deduction_Change,p.Overall_RTBL,p.Exposures,p.status,0,p.WrittenLine);

            pdb.SubmitChanges();
            ViewBag.result = "Policy created successfully";
            ViewBag.Smsg = "Workflow triggered to scoring system and an Email  with Policy review evaluation  will be sent to peer underwriter ";
            return View();


        }

    }
}