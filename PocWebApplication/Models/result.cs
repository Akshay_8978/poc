﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PocWebApplication.Models
{
    public class result
    {
        public string Attribute { get; set; }
        public string Value { get; set; }
        public string  Rating { get; set; }
    }
}