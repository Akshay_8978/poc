﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PocWebApplication.Models
{
    public class newRule
    {
        public string Division { get; set; }
        public string Business_Unit { get; set; }
        public string Attribute { get; set; }
        public string Value { get; set; }

        public string score { get; set; }
    }
}