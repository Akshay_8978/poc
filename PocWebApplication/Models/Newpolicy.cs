﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PocWebApplication.Models
{
    public class Newpolicy
    {
        public string Division { get; set; }

        public string BusinessUnit { get; set; }

        public string underWriteName { get; set; }

        public string u_wReference { get; set; }

        public string brokerCode { get; set; }

        public string assured { get; set; }

        public string writtenDate { get; set; }

        public DateTime? periodFrom { get; set; }

        public DateTime? periodTo { get; set; }

        public string Territory { get; set; }

        public string Limit { get; set; }

        public string Excess { get; set; }

        public int? Order { get; set; }

        public string Deductions { get; set; }

        public string Interest { get; set; }

        public int? Premium_RTBL { get; set; }

        public int? Claims_RTBL { get; set; }

        public int? LDRTBL { get; set; }

        public int? Deduction_Change { get; set; }

        public int? Overall_RTBL { get; set; }

        public string Exposures { get; set; }

        public string status { get; set; }

        public int? Flag { get; set; }

        public string WrittenLine { get; set; }
    }
}