﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PocWebApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
           // routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
           // routes.MapRoute(
           //    name: "addnewruleDefault",
           //    url: "{controller}/{action}/{s}",
           //    defaults: new { controller = "Poc", action = "addNewRule", s = UrlParameter.Optional }
           //);
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{s}",
                defaults: new { controller = "Poc", action = "addPolicy", s = UrlParameter.Optional }
            );
        }
    }
}
