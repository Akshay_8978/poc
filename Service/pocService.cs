﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using Poc_WebApi.Models;
using System.Text.RegularExpressions;
using System.Data.Entity.Validation;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using System.Linq;

namespace Poc_WebApi.Service
{
    public class pocService
    {
        POCDBEntity ch = new POCDBEntity();
        
      public  int divnumber;
       public int  bunumber;
        Regex floatreg = new Regex("^[0-9]{1,2}.[0-9]{1,2}$");
       Regex intreg = new Regex("^[0-9]{1,8}$");
        List<rule> rlist = new List<rule>();
     
        PocReport pr = new PocReport();
        List<result> _presult = new List<result>();
        

        /// 
      public  bool exdiv;
       public bool exbu;
        public bool asno;
        public bool atrvalue;

        public int dsno { get; set; }
        public  int  bsno { get; set; }
        public string bu_name { get; set; }
        public string d_name { get; set; }
        public int attserialno { get; set; }
        int scorevalue { get; set; }
        int newrules_sNo { get; set; }
        int newattr_sNo { get; set; }

         public rule newr = new rule();
        public attribute newa = new attribute();





        public PocReport pocreview(Dictionary<string,string> s)
        {
            try
            {
                foreach (KeyValuePair<string, string> val in s)
                {
                    foreach (var div in ch.divisions)
                    {
                        if (val.Value.Trim() == div.name.Trim())
                        {

                            foreach (KeyValuePair<string, string> v in s)
                            {
                                foreach (businessUnit bu in ch.businessUnits)
                                {
                                    if (v.Value.Trim() == bu.name.Trim())
                                    {
                                        bunumber = bu.sNO;
                                        divnumber= div.sNo;
                                        divbu _db = new divbu();
                                        _db.Division = div.name; _db.Business_Unit = bu.name;
                                        pr.db = _db;
                                      
                                        rlist = ch.rules.Where(x => (x.div_Sno == div.sNo) && (x.business_Sno == bu.sNO)).ToList();
                                        break;
                                    }
                                    //else
                                    //{

                                    //    throw new ObjectNotFoundException();
                                       

                                    //        }

                                }

                            }
                        }
                        //else { throw new ObjectNotFoundException(); }
                    }
                }

                foreach (KeyValuePair<string, string> kv in s)
                {
                    foreach (attribute attr in ch.attributes)
                    {
                        if (kv.Key.Trim() == attr.attributename.Trim())  // to check the attribute name in the attribute 
                        {

                            bool intregcheck = intreg.IsMatch(kv.Value.Trim());
                            bool floatregcheck = floatreg.IsMatch(kv.Value.Trim());

                            if (intregcheck)
                            {
                                List<rule> rulelist = new List<rule>();
                                rulelist = ch.rules.Where(x => (x.div_Sno == divnumber) && (x.business_Sno == bunumber) && (x.attribute_Sno == attr.sNo)).ToList();
                                foreach (rule ra in rulelist)
                                {
                                    string[] attr_array = ra.attribute_Value.Trim().Split('-');
                                    int min = int.Parse(attr_array[0]);
                                    int max = int.Parse(attr_array[1]);
                                    if ((int.Parse(kv.Value) >= min) && (int.Parse(kv.Value) <= max))
                                    {

                                        var rating = (from rate in ch.ratings

                                                      where rate.score.Equals(ra.score)
                                                      select rate.rating1).Single();
                                        result _r = new result();
                                        _r.Attribute = attr.attributename.Trim(); _r.Value = kv.Value; _r.Rating = rating;
                                        _presult.Add(_r);

                                        break;

                                    }

                                }

                            }
                            else if (floatregcheck)
                            {
                                List<rule> rulelist = new List<rule>();
                                rulelist = ch.rules.Where(x => (x.div_Sno == divnumber) && (x.business_Sno == bunumber) && (x.attribute_Sno == attr.sNo)).ToList();
                                foreach (rule ra in rulelist)
                                {

                                    string[] fattr_array = ra.attribute_Value.Trim().Split('-');
                                    float fmin = float.Parse(fattr_array[0]);
                                    float fmax = float.Parse(fattr_array[1]);
                                    if ((float.Parse(kv.Value) >= fmin) && (float.Parse(kv.Value) <= fmax))
                                    {

                                        var rating = (from rate in ch.ratings
                                                      where rate.score.Equals(ra.score)
                                                      select rate.rating1).Single();
                                        result _re = new result();
                                        _re.Attribute = attr.attributename.Trim(); _re.Value = kv.Value; _re.Rating = rating;
                                        _presult.Add(_re);
                                        break;

                                    }


                                }

                            }
                            else
                            {


                                var rating = (from rate in ch.ratings
                                              join rl in ch.rules on rate.score equals rl.score
                                              where (rl.div_Sno.Equals(divnumber) && (rl.business_Sno.Equals(bunumber) && (rl.attribute_Sno.Equals(attr.sNo)) && (rl.attribute_Value.Trim().Equals(kv.Value.Trim()))))
                                              select rate.rating1).Single();

                                result _res = new result();
                                _res.Attribute = attr.attributename.Trim(); _res.Value = kv.Value.Trim(); _res.Rating = rating.ToString();
                                _presult.Add(_res);
                                break;


                            }

                            pr.report = _presult;

                        }
                      

                    }

                }


                foreach (KeyValuePair<string,string> ka in s)
                {
                    if (!(ka.Key.Trim().Equals("Division",StringComparison.OrdinalIgnoreCase)) && !(ka.Key.Trim().Equals("BU",StringComparison.OrdinalIgnoreCase)))
                    {
                        bool ra = ch.attributes.Any(x => x.attributename.Trim().Equals(ka.Key.Trim()));
                        if (!ra)
                        {
                            result re = new result();
                            re.Attribute = ka.Key.Trim(); re.Value = ka.Value.Trim(); re.Rating = "Rule not defined for this data point";
                            _presult.Add(re);
                        }
                    }   

                }


                return pr;



            }
            catch (Exception ex )
            {

                throw new Exception(ex.Message);
            }

        }

        public bool addrule(Dictionary<string,string> n )
        {
            bool result = false;
            try
            {
             
                //// to find the division serial no 
                foreach (KeyValuePair<string, string> kdvalue in n)
                {
                    exdiv = ch.divisions.Any(x => x.name.Trim() == kdvalue.Value.Trim());
                    if (exdiv)
                    {
                        dsno = (from d in ch.divisions where d.name.Trim().Equals(kdvalue.Value.Trim()) select d.sNo).Single();
                        n.Remove(kdvalue.Key);
                        break;
                    }
                    else
                    {
                        d_name = kdvalue.Value.Trim();
                        n.Remove(kdvalue.Key);
                    }

                }
                //// to find the business unit serial no 
                foreach (KeyValuePair<string, string> kbvalue in n)
                {
                    exbu = ch.businessUnits.Any(x => (x.name.Trim() == kbvalue.Value.Trim()) && (x.divSno == dsno));
                    if (exbu)
                    {
                        bsno = (from d in ch.businessUnits where d.name.Trim().Equals(kbvalue.Value.Trim()) select d.sNO).Single();
                        n.Remove(kbvalue.Key);
                        break;
                    }
                    else
                    {
                        bu_name = kbvalue.Value;
                        n.Remove(kbvalue.Key);
                    }
                }
                /// to find the score value 
                foreach (KeyValuePair<string, string> Kscore in n)
                {
                    if (Kscore.Key.Trim() == "score" || Kscore.Key.Trim() == "Score")
                    {
                        scorevalue = (int)Convert.ToInt32(Kscore.Value.Trim());
                        break;
                    }
                }

                /// case: 1 where division and business unit already exist 
                if (exdiv && exbu)
                {

                    foreach (KeyValuePair<string, string> kattr in n)
                    {
                        asno = ch.attributes.Any(x => (x.attributename.Trim() == kattr.Key.Trim()));


                        if (asno)
                        {
                            attserialno = (from a in ch.attributes where a.attributename.Trim().Equals(kattr.Key.Trim()) select a.sNo).Single();
                          
                            // check whether the attribute value is present or not 
                            atrvalue = ch.rules.Any(x => (x.div_Sno.Equals(dsno)) && (x.business_Sno.Equals(bsno))
                                         && (x.attribute_Sno.Equals(attserialno)) && (x.attribute_Value.Trim().Equals(kattr.Value.Trim())));
                            if (!atrvalue)
                            {

                                newrules_sNo = (from sno in ch.rules select sno.sNo).Max() + 1;


                              
                                ch.insertrule(newrules_sNo,dsno,bsno,attserialno,kattr.Value.Trim(),scorevalue);
                                
                                result = true;
                                break;
                            }
                            break;

                        }
                        else if(!asno)
                        {
                            newattr_sNo = (from ano in ch.attributes select ano.sNo).Max() + 1;
                            newrules_sNo = (from sno in ch.rules select sno.sNo).Max() + 1;
                            ch.insertAttribute(newattr_sNo,kattr.Key.Trim());
                       
                            ch.insertrule(newrules_sNo,dsno,bsno,newattr_sNo,kattr.Value.Trim(),scorevalue);
                            result = true;
                            break;

                        }
                       
                    }

             

                }

                /////// case 2 if division exist and business unit doesnot exist 

                else if ((exdiv == true) && (exbu == false))
                {
                    int at_no;
                    int new_atno;
                    int new_r;



                    int newbsno = (from b in ch.businessUnits select b.sNO).Max() + 1;
                    ch.insertbu(newbsno,bu_name.Trim(),dsno);

                    foreach (KeyValuePair<string, string> at in n)
                    {
                        bool c = ch.attributes.Any(x => x.attributename.Trim().Equals(at.Key.Trim()));
                        if (c)
                        {
                            at_no = (from a in ch.attributes where a.attributename.Trim() == at.Key.Trim() select a.sNo).Single();

                         
                            new_r = (from sno in ch.rules select sno.sNo).Max() + 1;
                          
                            ch.insertrule(new_r,dsno,bsno,at_no,at.Value.Trim(),scorevalue);
                            result = true;
                            break;
                        }
                        else
                        {
                            new_atno = (from ano in ch.attributes select ano.sNo).Max() + 1;
                            new_r = (from sno in ch.rules select sno.sNo).Max() + 1;
                          
                            ch.insertAttribute(new_atno,at.Key.Trim());
                            ch.insertrule(new_r,dsno,bsno,new_atno,at.Value.Trim(),scorevalue);
                            result = true;
                            break;
                        }
                    }
                }

                //// case :3 if both div and bu are false 
                else if ((exdiv == false) && (exbu == false))
                {
                    int div_s; int b_s; int at_no;
                    int new_atno; int new_r;
                    div_s = (from d in ch.divisions select d.sNo).Max() + 1;
                    b_s = (from b in ch.businessUnits select b.sNO).Max() + 1;
                  
                    ch.insertdivision(div_s,d_name);
                    ch.insertbu(b_s,bu_name,div_s);
                    foreach (KeyValuePair<string, string> at in n)
                    {
                        bool c = ch.attributes.Any(x => x.attributename.Trim().Equals(at.Key.Trim()));
                        if (c)
                        {
                            at_no = (from a in ch.attributes where a.attributename.Trim() == at.Key.Trim() select a.sNo).Single();
                             
                            new_r = (from sno in ch.rules select sno.sNo).Max() + 1;
                          
                            ch.insertrule(new_r,dsno,bsno,at_no,at.Value.Trim(),scorevalue);
                            result = true;
                            break;
                        }
                        else
                        {
                            new_atno = (from ano in ch.attributes select ano.sNo).Max() + 1;
                            new_r = (from sno in ch.rules select sno.sNo).Max() + 1;
                       
                            ch.insertAttribute(new_atno,at.Key.Trim());
                            ch.insertrule(new_r,dsno,bsno,new_atno,at.Value.Trim(),scorevalue);
                            result = true;
                            break;
                        }
                    }


                }


                


            }


            catch (DbEntityValidationException ex )
            {

                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {


                        throw new Exception ("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                    }
                }
            }

            return result;

        }






    }

}