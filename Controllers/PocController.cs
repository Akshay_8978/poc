﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Poc_WebApi.Service;
using System.Runtime.Serialization;

namespace Poc_WebApi.Controllers
{

    public class PocController : ApiController
    {
        pocService _s = new pocService();

        [HttpGet]
        public IHttpActionResult poc([FromUri] Dictionary<string, string> s)
        {
            var l = _s.pocreview(s);
            if (l != null)
            { return Ok(l); }
            else
              return  BadRequest();
            
        }


        [HttpPost]
        public IHttpActionResult newrule([FromUri] Dictionary<string,string> a)
        {
            bool r = _s.addrule(a);
            if (r)
            {
                return Ok("Values are added to db");
            }
            else return BadRequest("given values are incorrect");
        }



    }
}
