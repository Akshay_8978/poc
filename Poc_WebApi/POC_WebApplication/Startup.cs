﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(POC_WebApplication.Startup))]
namespace POC_WebApplication
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
