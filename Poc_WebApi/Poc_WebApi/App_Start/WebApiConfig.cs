﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Poc_WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{s}",
                defaults: new {controller="Poc" , action="poc", s = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
               name: "Addnewattribute",
               routeTemplate: "api/{controller}/{action}/{a}",
               defaults: new { controller = "Poc", action = "newrule", a = RouteParameter.Optional }
           );
        }
    }
}
