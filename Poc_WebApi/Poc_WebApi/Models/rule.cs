//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Poc_WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class rule
    {
        public int sNo { get; set; }
        public int div_Sno { get; set; }
        public int business_Sno { get; set; }
        public int attribute_Sno { get; set; }
        public string attribute_Value { get; set; }
        public int score { get; set; }
    }
}
